﻿using System;
using CommandArtifactItemDrops.Enums;
using CommandArtifactItemDrops.Utilities;
using On.RoR2.Artifacts;
using RoR2;

namespace CommandArtifactItemDrops.Hooks
{
    /// <summary>
    /// The sacrifice artifact manager hooks
    /// </summary>
    public class SacrificeArtifactManagerHooks
    {
        /// <summary>
        /// Binds the event hooks
        /// </summary>
        public static void Bind()
        {
            Unbind();

            SacrificeArtifactManager.OnServerCharacterDeath += SacrificeArtifactManager_OnOnServerCharacterDeath;
        }

        /// <summary>
        /// Unbinds the event hooks
        /// </summary>
        private static void Unbind()
        {
            SacrificeArtifactManager.OnServerCharacterDeath -= SacrificeArtifactManager_OnOnServerCharacterDeath;
        }

        private static void SacrificeArtifactManager_OnOnServerCharacterDeath(
            SacrificeArtifactManager.orig_OnServerCharacterDeath orig,
            DamageReport damageReport)
        {
            // safe-guard the configurable drop chance percentage
            var dropChancePercent = CommandArtifactItemDropsPlugin.DropChancePercentageFromSacrificeArtifact.Value;
            if (dropChancePercent < 0 || dropChancePercent > 1)
            {
                dropChancePercent = Convert.ToSingle(CommandArtifactItemDropsPlugin.DropChancePercentageFromSacrificeArtifact.DefaultValue);
            }

            var swarmsArtifact = ArtifactCatalog.FindArtifactDef(nameof(ArtifactEnum.Swarms));
            if (RunArtifactManager.instance.IsArtifactEnabled(swarmsArtifact))
            {
                // cut the drop rate in half if swarms is enabled
                dropChancePercent /= 2;
            }

            // roll to see if check is successful
            var dropCoefficient = CommandArtifactItemDropsPlugin.Random.NextDouble();
            if (dropCoefficient < dropChancePercent)
            {
                // enable the command artifact
                CommandArtifactHelper.ToggleCommandArtifact(true);

                // we failed the drop coefficient for it, so drop a random item instead
                CommandArtifactItemDropsPlugin.Logger.LogMessage($"Command artifact temporarily enabled after slaying {damageReport.victimBody.name} with {dropChancePercent:P} chance!");
            }

            // invoke the original method
            orig.Invoke(damageReport);

            // disable the command artifact
            CommandArtifactHelper.ToggleCommandArtifact(false);
        }
    }
}
