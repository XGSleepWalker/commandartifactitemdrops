﻿using System;
using System.Collections.Generic;
using CommandArtifactItemDrops.Enums;
using CommandArtifactItemDrops.Utilities;
using RoR2;
using UnityEngine;
using DeathRewards = On.RoR2.DeathRewards;

namespace CommandArtifactItemDrops.Hooks
{
    public class DeathRewardHooks
    {
        /// <summary>
        /// Binds the event hooks
        /// </summary>
        public static void Bind()
        {
            Unbind();

            DeathRewards.OnKilledServer += DeathRewards_OnOnKilledServer;
        }

        /// <summary>
        /// Unbinds the event hooks
        /// </summary>
        private static void Unbind()
        {
            DeathRewards.OnKilledServer -= DeathRewards_OnOnKilledServer;
        }

        private static void DeathRewards_OnOnKilledServer(
            DeathRewards.orig_OnKilledServer orig,
            RoR2.DeathRewards self,
            DamageReport damageReport)
        {
            // get the sacrifice artifact definition
            var sacrificeArtifact = ArtifactCatalog.FindArtifactDef(nameof(ArtifactEnum.Sacrifice));

            // do not do anything if the sacrifice artifact is not enabled
            // or if the slain enemy was not a monster type
            if (!RunArtifactManager.instance.IsArtifactEnabled(sacrificeArtifact)
                || damageReport?.victimBody == null
                || damageReport.victimBody.teamComponent.teamIndex != TeamIndex.Monster)
            {
                // perform the original action
                orig.Invoke(self, damageReport);

                return;
            }

            // safe-guard the configurable drop chance percentage
            var dropChancePercent = CommandArtifactItemDropsPlugin.DropChancePercentageFromDeathRewards.Value;
            if (dropChancePercent < 0 || dropChancePercent > 1)
            {
                dropChancePercent = Convert.ToSingle(CommandArtifactItemDropsPlugin.DropChancePercentageFromDeathRewards.DefaultValue);
            }

            var swarmsArtifact = ArtifactCatalog.FindArtifactDef(nameof(ArtifactEnum.Swarms));
            if (RunArtifactManager.instance.IsArtifactEnabled(swarmsArtifact))
            {
                // cut the drop rate in half if swarms is enabled
                dropChancePercent /= 2;
            }

            // only drop the item chooser if the proc coefficient is successful
            var dropCoefficient = CommandArtifactItemDropsPlugin.Random.NextDouble();
            if (dropCoefficient < dropChancePercent)
            {
                CommandArtifactHelper.ToggleCommandArtifact(true);

                DropRandomItem(damageReport);

                CommandArtifactHelper.ToggleCommandArtifact(false);
            }
            else
            {
                // perform the original action
                orig.Invoke(self, damageReport);
            }
        }

        private static void DropRandomItem(
            DamageReport damageReport)
        {
            List<PickupIndex> dropList;

            // check to see which tier of item will be rewarded
            var dropCoefficient = CommandArtifactItemDropsPlugin.Random.NextDouble();
            if (dropCoefficient < CommandArtifactItemDropsPlugin.DefaultChanceFromDeathRewardsForRare)
            {
                // a rare item was rewarded
                dropList = Run.instance.availableTier3DropList;
            }
            else if (dropCoefficient < CommandArtifactItemDropsPlugin.DefaultChanceFromDeathRewardsForUncommon)
            {
                // an uncommon item was rewarded
                dropList = Run.instance.availableTier2DropList;
            }
            else
            {
                // a common item was rewarded
                dropList = Run.instance.availableTier1DropList;
            }

            // get a random item from the drop list
            var randomItem = dropList[CommandArtifactItemDropsPlugin.Random.Next(0, dropList.Count)];

            PickupDropletController.CreatePickupDroplet(randomItem, damageReport.victimBody.transform.position, new Vector3(UnityEngine.Random.Range(-5.0f, 5.0f), 20f, UnityEngine.Random.Range(-5.0f, 5.0f)));
        }
    }
}
