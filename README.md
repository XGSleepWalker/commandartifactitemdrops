﻿
# Command Artifact Item Drops

This mod adds a configurable chance for command artifact item pickers to drop in any run.  Note: this mod is enabled whether or not the command artifact itself is enabled and helps to balance its power.

## Installation

Install with a mod manager or drop the dll into your BepInEx directory.

## Configuration Settings

 - Chests
	 - Drop Chance Percentage - Sets the chance that a command artifact item picker will drop from chests. [0.00-1.00, default=0.10].

- Sacrifice Artifact
	 - Drop Upgrade Chance Percentage - Sets the chance that the drop from a slain monster will upgrade to a command artifact item picker when the sacrifice artifact is enabled and a drop occurs.[0.00-1.00, default=0.05].
	 - Drop Chance Percentage from Kill Rewards - Sets the chance that a command artifact item picker will drop from slain monsters as death rewards when the sacrifice artifact is enabled. [0.00-1.00, default=0.005].

## Version History

- 0.3.1
	- Corrects a bug that occurred when using the Sacrifice artifact along with BiggerBazaar while inside the bazaar.

- 0.3.0
	- Adds support for use with the Sacrifice artifact.

- 0.2.0
	- Corrects an unintentional interaction with the Sacrifice Artifact.

- 0.1.0
	- Initial revision.
