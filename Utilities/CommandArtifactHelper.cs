﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CommandArtifactItemDrops.Enums;
using R2API;
using RoR2;
using UnityEngine.SceneManagement;

namespace CommandArtifactItemDrops.Utilities
{
    /// <summary>
    /// The command artifact helper
    /// </summary>
    public static class CommandArtifactHelper
    {
        public static bool IsInBazaar()
        {
            var isInBazaar = SceneManager.GetActiveScene().name.Equals(DirectorAPI.Stage.Bazaar.ToString(), StringComparison.InvariantCultureIgnoreCase);
            return isInBazaar;
        }

        /// <summary>
        /// Toggles the enabled status of the command artifact
        /// </summary>
        /// <param name="isEnabled">A value indicating whether the command artifact
        /// will be enabled or disabled</param>
        public static void ToggleCommandArtifact(bool isEnabled)
        {
            // get the command artifact definition
            var commandArtifact = ArtifactCatalog.FindArtifactDef(nameof(ArtifactEnum.Command));

            if (RunArtifactManager.instance.IsArtifactEnabled(commandArtifact) == isEnabled)
            {
                // artifact status is not changing
                return;
            }

            if (isEnabled)
            {
                // enable the command artifact 
                RunArtifactManager.instance.SetArtifactEnabledServer(commandArtifact, true);
            }
            else
            {
                Task.Run(() =>
                {
                    // sleep for a short time before disabling the command artifact
                    Thread.Sleep(CommandArtifactItemDropsPlugin.CommandArtifactTimeout);

                    // disable the command artifact 
                    RunArtifactManager.instance.SetArtifactEnabledServer(commandArtifact, false);
                });
            }
        }
    }
}
